# Sprint 2 (Aug 12 - Aug 19)

* [x] R0.5-0.2: Merge the new accomplished changes of the last sprint into `main`
* [x] R0.2-0.2: Make descriptive names for the variables x, y, and z
* [x] R0.1-0.1: Create a new file called `settings.json`
* [x] R0.1-0.1: Add the first configurables to the `settings.json` file
* [x] R0.2-0.2: Install and integrate `@rollup/plugin-json` plugin in the app
> The tasks listed below can be divided into additional tasks
* [x] R3-2: Refactor/restucture the source code to make it easy to configure
* [x] R2-1.5: Create a configurable that turn on/off the greeting component
* [x] R2-1: Create a configurable that allow/prevent empty messages to be sent
* [x] R1-0.5: Install and run Nudger locally
* [X] R0.2-R0.2: Create a Svelte draft component within **faceofqary** project
* [x] R2-R1: Connect the draft component to the `nudger`'s web socket server
* [x] R1-0.5: Pass a fetch to get the the welcome message
* [x] R1-1: Make another call that responds to **English**
* [x] R1-1.5: Store the web socket response and update the current state 


## Done: Sprint 1 (Aug 05 - Aug 12)

* [x] R0.2-R0.1: Clone the [svelte-tawk](https://github.com/hellpirat/svelte-tawk-to-chat-widget) repository
* [x] R1-1: Adapt what is useful from the [tawk tutorial](https://raw.githubusercontent.com/codebubb/tawk-tutorial/main/src/Tawk.js) repo 
* [x] H?: Create a new repo named `faceofqary` and make it public
* [x] R0.1-0.1: Clone the `faceofqary` repository
* [x] R0.2-0.1: Edit the README file by adding the project plan below
* [x] R0.2-0.1: Create an empty Svelte project called **client**
* [x] R0.2-0.1: Add a `.gitignore` file to ignore node_modules and build files
* [x] R0.2-0.1: Push the empty svelte project to the `faceofqary` repository
* [x] R3-R2.5: Build a chat widget component using Svelte
* [x] R1-1: Create a chat-window component that contains an empty text box and chat header
* [x] R1-1: Add a **send** that sends messages
* [x] R0.5-0.5: Up-scroll previous messages in the same window
* [x] R0.5-0.5: Make the **chat conversation** button initialize the chat (welcome message)
* [x] R0.5-1: Make the chat bullet echo what the user says in the chat 
* [x] R0.5-0.5: Delay the welcome message
* [x] R0.5-0.5: Edit the **start conversation** button when the chat starts
* [x] R0.5-0.2: Create an empty django project in the same repository
* [x] R1-0.5: Structure the repository using the monorepo multi-server approach
* [x] R1-0.5: Create two web services called **faceofqary-front** and **faceofqary-back**
* [x] R1-1: Deploy the application on Render one step at a time

