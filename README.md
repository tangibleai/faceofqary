# face-of-qary

Chameleon is an open source chat widget built with Svelte and powered by Tangible AI.

## Walkthrough

Please follow the instructions below to embed our chat widget into your own website.

1. Copy and paste the stylesheet link and the script tag below into your `<head>` tag of your HTML file before all other stylesheets to load properly our CSS:

```html
<head>
  <link
    rel="stylesheet"
    href="https://faceofqary-front.onrender.com/build/bundle.css"
  />
  <script
    defer
    src="https://faceofqary-front.onrender.com/build/bundle.js"
  ></script>
</head>
```

2. Some of our widget components are styled with Bootstrap and use the Bootstrap's free open source icon library. Place the following links into your `<head>` tag after the previous ones:

```html
<head>
  <link
    href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
    rel="stylesheet"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
    crossorigin="anonymous"
  />
  <link
    rel="stylesheet"
    href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css"
  />
</head>
```

3. Copy and paste the following `<script>` tag before the closing `<body>` tag to enable Bootstrap JavaScript plugins and Popper:

```html
<script
  src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
  integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
  crossorigin="anonymous"
></script>
```

4. Optionally, our widget uses Ubuntu from [Google Fonts](https://fonts.google.com/) as a web font. Feel free to choose your favorite font and inlcude it in your CSS file or in your `<head>` if it is embedded fron an external source:

```html
<link rel="preconnect" href="https://fonts.googleapis.com" />
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
<link
  href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap"
  rel="stylesheet"
/>
```

5. If we put all together, your page should look closer to this:

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- CSS Bundle -->
        <link rel='stylesheet' href='https://faceofqary-front.onrender.com/build/bundle.css'>
        <!-- JS Bundle -->
        <script defer src = "https://faceofqary-front.onrender.com/build/bundle.js"></script>
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <!-- Bootstrap icons -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">
        <!-- Google Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
    <head>
    <body>
        <!-- YOUR HTML CODE HERE -->
        <!-- Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    </body>
</html>
```

Svelte chatbot widget for use with Tangible AI's `qary.ai`, `nudger`, or `moia-poly-chatbot` Django Channels (websockets) backend.
Backend is made stateless (REST) by sending and receiving user_id (room_id), state_name, and other context information with every websocket message using a `context` nested dictionary.
Use NPM to create a new bundle.js that can be deployed anywhere.

commands to create the bundle (assume you are in the project root):

```
cd client
npm init -y
npm install @rollup/plugin-commonjs @rollup/plugin-json @rollup/plugin-node-resolve rollup rollup-plugin-css-only rollup-plugin-livereload rollup-plugin-svelte rollup-plugin-terser sirv-cli svelte svelte-check bootstrap bootstrap-icons svelte-navigator --save
npm install prettier prettier-plugin-svelte --save-dev
npm run build
```

Then you will see files called `bootstrap.min.css.map`, `polyface.css`, `polyface.js` and `polyface.js.map` placed in `client/public/build` subdirectory. Use them anywhere you want to see bundle in action

## **FIXME:**

- [ ] Don't require buttons to be present (allow text-only chatbot dialog to work)

## **TODO:**

- `config.json` with `backend_fqdn` (fully qualified domain name such as `qary.ai`)
- `config.json` with icon image URL
- `config.json` with `backend_paths` (endpoint URL path dict such as {'register': 'api/register', 'send': 'api/send'})
- `config.json` with `backend_scheme` (`https://`, `http://`, `ws://`, or `wss://` name such as `qary.ai`)
- `config.json` with default start `state_name`
- `config.json` with default `lang` such as `en`.

## Resources

- [Poly on We Speak NYC homepage](https://wespeaknyc.cityofnewyork.us/) - [typescript source code](https://gitlab.com/tangibleai/moia-poly-chatbot) by [Clynton Caines](https://github.com/clynton)
- [Dynamic Django (python) backend-only Poly implementation](https://www.qary.ai/poly/) - [source code](https://gitlab.com/tangibleai/nudger)
- [Educational "Regression" Game](https://playground.proai.org/) - [svelte source code](https://gitlab.com/tangibleai/django-svelte-ml-playground)
- [Svelte REPL](https://svelte.dev/repl)
