rm public/build/polyface.css public/build/polyface.js public/build/polyface.js.map
npm run build
rm ~/tangibleai/nudger/static/polyface/polyface.css ~/tangibleai/nudger/static/polyface/polyface.js ~/tangibleai/nudger/static/polyface/polyface.js.map
cp public/build/polyface.css ~/tangibleai/nudger/static/polyface/
cp public/build/polyface.js ~/tangibleai/nudger/static/polyface/
cp public/build/polyface.js.map ~/tangibleai/nudger/static/polyface/